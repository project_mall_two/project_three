package com.mall.shopping.bootstrap;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.mall.shopping.converter.CartItemConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dto.*;
import com.mall.shopping.*;
import com.mall.shopping.services.cache.CacheManager;
import com.mall.shopping.utils.CartUtils;
import org.checkerframework.checker.units.qual.C;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShoppingProviderApplicationTests {
    @Autowired
    CacheManager cacheManager;
    @Autowired
    ItemMapper itemMapper;

    @Test
    public void addToCart() {
        AddCartRequest request = new AddCartRequest();
        request.setNum(3);
        request.setItemId((long) 100023501);
        request.setUserId((long) 63);
        String userId = String.valueOf(request.getUserId());
        String cacheValue = cacheManager.checkCache(CartUtils.FORECODETAG + userId);
        if (cacheValue != null) {
            //如果检测到cache中存在对应的key则将数据库中的值添加值末尾
            Item item = itemMapper.selectByPrimaryKey(request.getItemId());
            CartProductDto cartProductDto = CartItemConverter.item2Dto(item);
            cartProductDto.setProductNum(request.getNum().longValue());
            if (cacheValue.contains(String.valueOf(request.getItemId()))) {
                //如果商品已经在购物车中存在
                List<CartProductDto> list = JSON.parseArray(cacheValue, CartProductDto.class);
                for (CartProductDto cartProductDto1 : list) {
                    if (cartProductDto1.getProductId().equals(request.getItemId())) {
                        Long Countnum = cartProductDto1.getProductNum() + request.getNum();
                        //根据两次的数值之和来和LimiNum进行判断
                        if (Countnum > cartProductDto1.getLimitNum()) {
                            cartProductDto1.setProductNum(cartProductDto1.getLimitNum());
                        } else {
                            cartProductDto1.setProductNum(Countnum);
                        }
                        break;
                    }
                }
                cacheManager.setCache(CartUtils.FORECODETAG + userId, new Gson().toJson(list), CartUtils.EXPRISE);
            } else {
                //如果商品在购物车中不存在
                //去除首尾的[ ]括号
                cacheValue = cacheValue.substring(1, cacheValue.length() - 1);
                cacheValue = cacheValue + "," + new Gson().toJson(cartProductDto);
                cacheValue = "[" + cacheValue + "]";
                cacheManager.setCache(CartUtils.FORECODETAG + userId, cacheValue, CartUtils.EXPRISE);
            }

        } else {
            //如果检测到cache中不存在对应的key则从数据库中取
            Item item = itemMapper.selectByPrimaryKey(request.getItemId());
            CartProductDto cartProductDto = CartItemConverter.item2Dto(item);
            cartProductDto.setProductNum(request.getNum().longValue());
            List<CartProductDto> cartProductDtoList = new ArrayList<>();
            cartProductDtoList.add(cartProductDto);
//            cacheValue = new Gson().toJson(cartProductDto);
//            cacheValue = "["+cacheValue+"]";
            cacheManager.setCache(CartUtils.FORECODETAG + userId, cacheValue, CartUtils.EXPRISE);
        }
        System.out.println();
    }
}
