package com.mall.order.bootstrap;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.mall.order.biz.TransOutboundInvoker;
import com.mall.order.biz.context.AbsTransHandlerContext;
import com.mall.order.biz.factory.OrderProcessPipelineFactory;
import com.mall.order.dal.entitys.Stock;
import com.mall.order.dal.persistence.StockMapper;
import com.mall.order.dto.CreateOrderRequest;
import com.mall.order.dto.CreateOrderResponse;
import com.mall.order.utils.ExceptionProcessorUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

/**
 * @author: jia.xue
 * @create: 2020-02-19 13:07
 * @Description
 **/
//	{ "tel":"18782059038", "userName":"admin", "streetName":"上海⻘浦区汇联路"
//			,
//			"cartProductDtoList":[ { "productId":100057401, "salePrice":149, "productNum":1,
//			"limitNum":100, "checked":"true", "productName":"Smartisan T恤 迪特拉姆斯"
//			,
//			"productImg":"https://resource.smartisan.com/resource/005c65324724692f7c9ba2fc7738db
//		13.png" } ], "addressId":5, "orderTotal":149 }
/*
public class StockTest extends OrderProviderApplicationTests{

    @Autowired
    private StockMapper stockMapper;
    @Autowired
    OrderProcessPipelineFactory orderProcessPipelineFactory;
    @Test
    public void test01(){
        CreateOrderRequest request =new CreateOrderRequest();
//        request.setAddressId((long) 5);
//        request.setUserName("admin");
//        request.setStreetName("上海⻘浦区汇联路");
//        request.setCartProductDtoList();
//        request.set();
//        request.setUserName();
        CreateOrderResponse response = new CreateOrderResponse();
        try {
            //创建pipeline对象
            TransOutboundInvoker invoker = orderProcessPipelineFactory.build(request);

            //启动pipeline
            invoker.start(); //启动流程（pipeline来处理）

            //获取处理结果
            AbsTransHandlerContext context = invoker.getContext();

            //把处理结果转换为response
            response = (CreateOrderResponse) context.getConvert().convertCtx2Respond(context);
        } catch (Exception e) {
//            log.error("OrderCoreServiceImpl.createOrder Occur Exception :" + e);
            ExceptionProcessorUtils.wrapperHandlerException(response, e);
        }
        System.out.println(new Gson().toJson(response));
    }
    }
}*/
