package com.mall.shopping.services;


import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.mall.shopping.ICartService;
import com.mall.shopping.converter.CartItemConverter;
import com.mall.shopping.converter.ProductConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dto.*;
import com.mall.shopping.services.cache.CacheManager;
import com.mall.shopping.utils.CartUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname ICartServiceImpl
 * @Description TODO
 * @Date 2020/6/13 2:03
 * @Created by Cyuer
 */
@Slf4j
@Component
@Service
public class ICartServiceImpl implements ICartService {
    @Autowired
    CacheManager cacheManager;
    @Autowired
    ItemMapper itemMapper;


    @Override
    public CartListByIdResponse getCartListById(CartListByIdRequest request) {
        String userId = String.valueOf(request.getUserId());
        String cacheValue = cacheManager.checkCache(CartUtils.FORECODETAG + userId);
        CartListByIdResponse cartListByIdResponse =new CartListByIdResponse();
        if(cacheValue!=null&&!cacheValue.isEmpty()){
            //如果购物车里面不为空则将商品转换程List返回
            List<CartProductDto> list = JSON.parseArray(cacheValue, CartProductDto.class);
            cartListByIdResponse.setCartProductDtos(list);
            return cartListByIdResponse;
        }
        return cartListByIdResponse;
    }

    @Override
    public AddCartResponse addToCart(AddCartRequest request) {
        String userId = String.valueOf(request.getUserId());
        String cacheValue = cacheManager.checkCache(CartUtils.FORECODETAG + userId);
        if (cacheValue != null&&!cacheValue.isEmpty()) {
            //如果检测到cache中存在对应的key则将数据库中的值添加值末尾
            Item item = itemMapper.selectByPrimaryKey(request.getItemId());
            CartProductDto cartProductDto = CartItemConverter.item2Dto(item);
            cartProductDto.setProductNum(request.getNum().longValue());
            cartProductDto.setChecked("false");
            if (cacheValue.contains(String.valueOf(request.getItemId()))) {
                //如果商品已经在购物车中存在
                List<CartProductDto> list = JSON.parseArray(cacheValue, CartProductDto.class);
                for (CartProductDto cartProductDto1 : list) {
                    if (cartProductDto1.getProductId().equals(request.getItemId())) {
                        Long Countnum = cartProductDto1.getProductNum() + request.getNum();
                        //根据两次的数值之和来和LimitNum进行判断
                        if (Countnum > cartProductDto1.getLimitNum()) {
                            cartProductDto1.setProductNum(cartProductDto1.getLimitNum());
                        } else {
                            cartProductDto1.setProductNum(Countnum);
                        }
                        break;
                    }
                }
                cacheManager.setCache(CartUtils.FORECODETAG + userId, new Gson().toJson(list), CartUtils.EXPRISE);
            } else {
                //如果商品在购物车中不存在，直接添加到cache中value的队尾
                //去除首尾的[ ]括号
                cacheValue = cacheValue.substring(1, cacheValue.length() - 1);
                cacheValue = cacheValue + "," + new Gson().toJson(cartProductDto);
                cacheValue = "[" + cacheValue + "]";
                cacheManager.setCache(CartUtils.FORECODETAG + userId, cacheValue, CartUtils.EXPRISE);
            }

        } else {
            //如果检测到cache中不存在对应的key则从数据库中取
            Item item = itemMapper.selectByPrimaryKey(request.getItemId());
            CartProductDto cartProductDto = CartItemConverter.item2Dto(item);
            cartProductDto.setProductNum(request.getNum().longValue());
            List<CartProductDto> cartProductDtoList = new ArrayList<>();
            cartProductDtoList.add(cartProductDto);
            cacheValue = new Gson().toJson(cartProductDtoList);
            cacheManager.setCache(CartUtils.FORECODETAG + userId, cacheValue, CartUtils.EXPRISE);
        }
        return new AddCartResponse();
    }

    @Override
    public UpdateCartNumResponse updateCartNum(UpdateCartNumRequest request) {
        String userId = String.valueOf(request.getUserId());
        String cacheValue = cacheManager.checkCache(CartUtils.FORECODETAG + userId);
        List<CartProductDto> list = JSON.parseArray(cacheValue, CartProductDto.class);
        //对其中Id相同的的字段进行更新
        for (CartProductDto cartProductDto1 : list) {
            if (cartProductDto1.getProductId().equals(request.getItemId())) {
                list.remove(cartProductDto1);
                cartProductDto1.setChecked(request.getChecked());
                cartProductDto1.setProductNum(request.getNum().longValue());
                list.add(cartProductDto1);
                break;
            }
        }
        cacheManager.setCache(CartUtils.FORECODETAG + userId, new Gson().toJson(list), CartUtils.EXPRISE);
        return new UpdateCartNumResponse();

    }
    private boolean cacheEmpty(List list){
        return  list.isEmpty();
    }

    @Override
    public CheckAllItemResponse checkAllCartItem(CheckAllItemRequest request) {
        return null;
    }

    @Override
    public DeleteCartItemResponse deleteCartItem(DeleteCartItemRequest request) {
        String userId = String.valueOf(request.getUserId());
        String cacheValue = cacheManager.checkCache(CartUtils.FORECODETAG + userId);
        List<CartProductDto> list = JSON.parseArray(cacheValue, CartProductDto.class);
        //查找其中productid相同的字段删除
        for (CartProductDto cartProductDto1 : list) {
            if (cartProductDto1.getProductId().equals(request.getItemId())) {
               list.remove(cartProductDto1);
               break;
            }
        }
        cacheManager.setCache(CartUtils.FORECODETAG + userId, new Gson().toJson(list), CartUtils.EXPRISE);
        return new DeleteCartItemResponse();
    }

    @Override
    public DeleteCheckedItemResposne deleteCheckedItem(DeleteCheckedItemRequest request) {
        String userId = String.valueOf(request.getUserId());
        String cacheValue = cacheManager.checkCache(CartUtils.FORECODETAG + userId);
        List<CartProductDto> list = JSON.parseArray(cacheValue, CartProductDto.class);
        //查找其中productid相同的字段删除
        for (CartProductDto cartProductDto1 : list) {
            if (cartProductDto1.getChecked().contains("true") ){
                list.remove(cartProductDto1);
            }
        }
        cacheManager.setCache(CartUtils.FORECODETAG + userId, new Gson().toJson(list), CartUtils.EXPRISE);
        return new DeleteCheckedItemResposne();
    }

    @Override
    public ClearCartItemResponse clearCartItemByUserID(ClearCartItemRequest request) {
        String userId = String.valueOf(request.getUserId());
        String cacheValue = cacheManager.checkCache(CartUtils.FORECODETAG + userId);
        if (cacheValue != null && !cacheValue.isEmpty()) {
            List<Long> productIds = request.getProductIds();
            List<CartProductDto> list = JSON.parseArray(cacheValue, CartProductDto.class);
            for (Long id : productIds) {
                for (CartProductDto cartProductDto1 : list) {
                    if (cartProductDto1.getProductId() == id) {
                        list.remove(cartProductDto1);
                        break;
                    }
                }
            }
            cacheManager.setCache(CartUtils.FORECODETAG + userId, new Gson().toJson(list), CartUtils.EXPRISE);
        }
        return new ClearCartItemResponse();
    }
}
