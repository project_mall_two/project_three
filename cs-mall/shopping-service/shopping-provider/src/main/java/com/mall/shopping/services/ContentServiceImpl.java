package com.mall.shopping.services;

import com.alibaba.fastjson.JSON;
import com.mall.shopping.IContentService;
import com.mall.shopping.constant.GlobalConstants;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.dal.entitys.PanelContent;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dto.NavListResponse;
import com.mall.shopping.dto.PanelContentDto;
import com.mall.shopping.services.cache.CacheManager;
import com.mall.shopping.utils.ExceptionProcessorUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Slf4j
@Service
@Component
public class ContentServiceImpl implements IContentService {

    @Autowired
    PanelContentMapper panelContentMapper;

    @Autowired
    ContentConverter contentConverter;

    @Autowired
    CacheManager cacheManager;

    @Override
    public NavListResponse queryNavList() {
        NavListResponse response=new NavListResponse();
        try {
            String cacheValue = cacheManager.checkCache(GlobalConstants.HEADER_PANEL_CACHE_KEY);
            if(cacheValue!=null&&!cacheValue.isEmpty()){
                //如果缓存中存在直接在缓存中获取
                List<PanelContentDto> list = JSON.parseArray(cacheValue,PanelContentDto.class);
                response.setPannelContentDtos(list);
                response.setCode(ShoppingRetCode.SUCCESS.getCode());
                response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
            }else {
                //否则从数据库中获得
                Example exampleContent = new Example(PanelContent.class);
                exampleContent.setOrderByClause("sort_order");
                Example.Criteria criteriaContent = exampleContent.createCriteria();
                criteriaContent.andEqualTo("panelId", GlobalConstants.HEADER_PANEL_ID);
                List<PanelContent> pannelContents = panelContentMapper.selectByExample(exampleContent);
                //添加缓存操作
                cacheManager.setCache(GlobalConstants.HEADER_PANEL_CACHE_KEY,JSON.toJSONString(pannelContents),3);

                response.setPannelContentDtos(contentConverter.panelContents2Dto(pannelContents));
                response.setCode(ShoppingRetCode.SUCCESS.getCode());
                response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
            }
        }catch (Exception e){
            log.error("ContentServiceImpl.queryNavList Occur Exception :"+e);
            ExceptionProcessorUtils.wrapperHandlerException(response,e);
        }
        return response;
    }
}
