package com.mall.shopping.services;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mall.shopping.ShopingHomepageService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dal.persistence.PanelMapper;
import com.mall.shopping.dto.HomePageResponse;
import com.mall.shopping.dto.PanelContentItemDto;
import com.mall.shopping.dto.PanelDto;
import com.mall.shopping.utils.ExceptionProcessorUtils;
import jdk.nashorn.internal.ir.annotations.Reference;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.SQL;
import org.apache.ibatis.session.SqlSessionException;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.apache.dubbo.config.annotation.Service;
import tk.mybatis.mapper.entity.Example;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * @Classname ShopingHomepageServiceImpl
 * @Description TODO
 * @Date 2020/6/11 17:11
 * @Created by Cyuer
 */
@Slf4j
@Component
@Service
public class ShopingHomepageServiceImpl implements ShopingHomepageService {
    @Autowired
    PanelMapper panelMapper;
    @Autowired
    PanelContentMapper panelContentMapper;
    @Autowired
    ContentConverter contentConverter;
    @Override
    public HomePageResponse ShoppingHomepage() {
//        try {
//            /*
//             * 获取表的Criteria对象使用
//             * */
            Example example = new Example(Panel.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("position", 0);
            criteria.andEqualTo("status", 1);
            example.setOrderByClause("sort_order");
            List<Panel> panels = panelMapper.selectByExample(example);
            //provoder.dal.entitys ----->dto
            List<PanelDto> panelDtos = contentConverter.panels2Dto(panels);

            for (PanelDto p : panelDtos) {
                //根据panel表中limitnum来限制查询的内容数量
                PageHelper.startPage(0, p.getLimitNum());
                //provoder.dal.entitys
                List<PanelContentItem> panelContentItems = panelContentMapper.selectPanelContentAndProductWithPanelId(p.getId());
                PageInfo<PanelContentItem> pageInfo = new PageInfo<>(panelContentItems);

                //将查询好的list数据转换程dto发送到前端
                List<PanelContentItemDto> panelContentItemDtos = contentConverter.panelContentItem2Dto(panelContentItems);
                p.setPanelContentItems(panelContentItemDtos);
            }

            //对返回数据类型进行封装 获取数据成功
            HomePageResponse homePageResponse = new HomePageResponse();
            homePageResponse.setPanelContentItemDtos(panelDtos);
            homePageResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            homePageResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
            return homePageResponse;
//        }catch (Exception e){
//            //捕捉异常如果发生异常返回，由于是select不需要进行回滚
//            HomePageResponse homePageResponse = new HomePageResponse();
//            ExceptionProcessorUtils.wrapperHandlerException(homePageResponse,e);
//            return homePageResponse;
//        }
    }
}
