package com.mall.shopping.services;

import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dal.persistence.PanelMapper;
import org.apache.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mall.shopping.IProductService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ProductConverter;
import com.mall.shopping.converter.ProductDetailConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.entitys.ItemDesc;
import com.mall.shopping.dal.persistence.ItemDescMapper;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tk.mybatis.mapper.entity.Example;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Classname IProductServiceImpl
 * @Description TODO
 * @Date 2020/6/12 14:47
 * @Created by Cyuer
 */
@Slf4j
@Component
@Service
public class IProductServiceImpl implements IProductService {
    @Autowired
    ItemMapper itemMapper;
    @Autowired
    ItemDescMapper itemDescMapper;
    @Autowired
    ProductConverter productConverter;
    @Autowired
    ContentConverter contentConverter;
    @Autowired
    PanelMapper panelMapper;
    @Autowired
    PanelContentMapper panelContentMapper;

    @Override
    public ProductDetailResponse getProductDetail(ProductDetailRequest request) {
        Long id= request.getId();
        Item item = itemMapper.selectByPrimaryKey(id);
        ItemDesc itemDesc = itemDescMapper.selectByPrimaryKey(id);
        ProductDetailDto productDetailDto = ProductDetailConverter.item2Dto(item,itemDesc);
        ProductDetailResponse productDetailResponse =new ProductDetailResponse();
        productDetailResponse.setProductDetailDto(productDetailDto);
        return productDetailResponse;
    }

    @Override
    public AllProductResponse getAllProduct(AllProductRequest request) {
        PageHelper.startPage(request.getPage(),request.getSize(),true);
        Example example =new Example(Item.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("status",1);
        String sort = request.getSort();
        //根据前端输入的sort判断是否进行排序
        if(sort!=null && !sort.isEmpty()){
            Integer type = Integer.valueOf(sort);
            if(type == -1 ){
                example.setOrderByClause("price"+" "+"desc");
            }
            if(type == 1 ){
                example.setOrderByClause("price");
            }
        }
        //查询的最大价格存在时需要查询位于最大价格和最小价格之间的商品
        if(request.getPriceLte()!=null){
            criteria.andBetween("price",request.getPriceGt(),request.getPriceLte());
        }
        else {
            criteria.andGreaterThan("price",request.getPriceGt());
        }
        List<Item> items = itemMapper.selectByExample(example);
        PageInfo<Item> itemPageInfo =new PageInfo<>(items);
        List<ProductDto> dtos = productConverter.items2Dto(items);
        AllProductResponse response = new AllProductResponse();
        response.setCode(ShoppingRetCode.SUCCESS.getCode());
        response.setCode(ShoppingRetCode.SUCCESS.getMessage());
        response.setTotal(itemPageInfo.getTotal());
        response.setProductDtoList(dtos);
        return response;
    }

    @Override
    public RecommendResponse getRecommendGoods() {
        RecommendResponse recommendResponse = new RecommendResponse();
        List<Panel> panelList = panelMapper.selectPanelIdByRemark("热门推荐");
        Set<PanelDto> set = new HashSet<>();
        if (panelList.size() == 0) {
            recommendResponse.setPanelContentItemDtos(set);
            return recommendResponse;
        }
        //获取id
        for (int i = 0; i < panelList.size(); i++) {
            Integer id = panelList.get(0).getId();
            //获取推荐品类
            List<PanelContentItem> panelContentItems = panelContentMapper.selectPanelContentAndProductWithPanelId(id);
            Panel panel = panelList.get(i);
            panel.setPanelContentItems(panelContentItems);
            PanelDto panelDto = contentConverter.panel2Dto(panel);
            set.add(panelDto);
        }
        recommendResponse.setPanelContentItemDtos(set);
        return recommendResponse;
    }
}
