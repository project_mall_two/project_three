package com.mall.user;

import com.mall.user.dto.UserLoginRequest;
import com.mall.user.dto.UserLoginResponse;
import com.mall.user.dto.UserRegisterRequest;
import com.mall.user.dto.UserRegisterResponse;

/**
 * @Classname IRegisterService
 * @Description TODO
 * @Date 2020/6/14 15:53
 * @Created by Cyuer
 */
public interface IRegisterService {


    UserRegisterResponse register(UserRegisterRequest registerRequest);
}
