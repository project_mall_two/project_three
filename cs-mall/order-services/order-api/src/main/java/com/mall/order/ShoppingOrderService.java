package com.mall.order;

import com.mall.order.dto.OrderItemRequest;
import com.mall.order.dto.OrderItemResponse;
import com.mall.order.dto.ShoppingOrderDto;

/**
 * @Classname ShoppingOrderService
 * @Description TODO
 * @Date 2020/6/12 17:16
 * @Created by Cyuer
 */
public interface ShoppingOrderService {
    ShoppingOrderDto ShopingOrder(OrderItemRequest request, String userinfo);
}
