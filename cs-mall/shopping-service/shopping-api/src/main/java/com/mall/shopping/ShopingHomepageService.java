package com.mall.shopping;

import com.mall.shopping.dto.HomePageResponse;

/**
 * @Classname ShopingHomepageService
 * @Description TODO
 * @Date 2020/6/11 16:22
 * @Created by Cyuer
 */
public interface ShopingHomepageService {
    /**
     * @Description  ：商品界面显示
     * @author       : Cyuer
     * @param        :
     * @return       :
     * @exception    :
     * @date         : 2020/6/11 17:09
     */
    HomePageResponse ShoppingHomepage();
}
