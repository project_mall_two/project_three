package com.mall.shopping.bootstrap;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.gson.Gson;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ProductConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dto.*;
import com.mall.shopping.services.cache.CacheManager;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Classname getAllProductTest
 * @Description TODO
 * @Date 2020/6/12 15:18
 * @Created by Cyuer
 */

/*{[{
        "productId": 100053312,
        "salePrice": 299.00,
        "productNum": 1,
        "limitNum": 100,
        "checked": "false",
        "productName": "地平线 8 号旅行箱",
        "productImg": "https://resource.smartisan.com/resource/db4895e45ee6f3339037dbf7200e63f2.png"
        }, {
        "productId": 100057501,
        "salePrice": 149.00,
        "productNum": 1,
        "limitNum": 100,
        "checked": "false",
        "productName": "Smartisan T恤 毕加索",
        "productImg": "https://resource.smartisan.com/resource/e9cd634b62470713f6b9c5a6065f4a10.jpg"
        }, {
        "productId": 100057601,
        "salePrice": 149.00,
        "productNum": 3,
        "limitNum": 100,
        "checked": "false",
        "productName": "Smartisan T恤 皇帝的新装",
        "productImg": "https://resource.smartisan.com/resource/d9586f7c5bb4578e3128de77a13e4d85.png"
        }, {
        "productId": 100053202,
        "salePrice": 999.00,
        "productNum": 1,
        "limitNum": 100,
        "checked": "false",
        "productName": "地平线 8 号商务旅行箱",
        "productImg": "https://resource.smartisan.com/resource/d1dcca9144e8d13ffb33026148599d0a.png"
        }, {
        "productId": 100057701,
        "salePrice": 149.00,
        "productNum": 3,
        "limitNum": 100,
        "checked": "false",
        "productName": "Smartisan T恤 丑小鸭",
        "productImg": "https://resource.smartisan.com/resource/c23837ddfa3de0103be11bcbbb744066.png"
        }]}*/
public class getAllProductTest extends ShoppingProviderApplicationTests {

    @Autowired
    ItemMapper itemMapper;
    @Autowired
    ProductConverter productConverter;
    @Autowired
    CacheManager cacheManager;
    @Test
    public void getAllProductTest() {
        AllProductRequest request =new AllProductRequest();
        request.setSort("1");
        request.setSize(20);
        request.setPage(1);
        request.setPriceGt(0.0);
        request.setPriceLte(null);
        PageHelper.startPage(request.getPage(),request.getSize(),true);
        Example example =new Example(Item.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("status",1);
        String sort = request.getSort();
        //根据前端输入的sort判断是否进行排序
        if(sort!=null || !sort.isEmpty()){
            Integer type = Integer.valueOf(sort);
            if(type == -1 ){
                example.setOrderByClause("price"+" "+"desc");
            }
            if(type == 1 ){
                example.setOrderByClause("price");
            }
        }
        //查询的最大价格存在时需要查询位于最大价格和最小价格之间的商品
        if(request.getPriceLte()!=null){
            criteria.andBetween("price",request.getPriceGt(),request.getPriceLte());
        }
        else {
            criteria.andGreaterThan("price",request.getPriceGt());
        }
        List<Item> items = itemMapper.selectByExample(example);
        PageInfo<Item> itemPageInfo =new PageInfo<>(items);
        List<ProductDto> dtos = productConverter.items2Dto(items);
        AllProductResponse response = new AllProductResponse();
        response.setCode(ShoppingRetCode.SUCCESS.getCode());
        response.setCode(ShoppingRetCode.SUCCESS.getMessage());
        response.setTotal(itemPageInfo.getTotal());
        response.setProductDtoList(dtos);
        System.out.println(new Gson().toJson(response));

    }
}
