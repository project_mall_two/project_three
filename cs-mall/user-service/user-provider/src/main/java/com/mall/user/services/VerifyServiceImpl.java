package com.mall.user.services;

import com.mall.user.IVerifyService;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dal.entitys.Member;
import com.mall.user.dal.entitys.UserVerify;
import com.mall.user.dal.persistence.MemberMapper;
import com.mall.user.dal.persistence.UserVerifyMapper;
import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Author lee
 * @Date 2020/6/12 22:57
 * @Version 1.0
 */
@Service
public class VerifyServiceImpl implements IVerifyService {

    @Autowired
    private UserVerifyMapper userVerifyMapper;

    @Autowired
    private MemberMapper memberMapper;

    @Override
    public UserVerifyResponse verify(UserVerifyRequest request) {
        UserVerifyResponse response = new UserVerifyResponse();
        //判空验证
        request.requestCheck();

        //根据uuid查询user_verify获取username
        Example example = new Example(UserVerify.class);
        example.createCriteria().andEqualTo("uuid",request.getUuid());
        List<UserVerify> userVerifies = userVerifyMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(userVerifies)){
            response.setCode(SysRetCodeConstants.USER_INFOR_INVALID.getCode());
            response.setMsg(SysRetCodeConstants.USER_INFOR_INVALID.getMessage());
            return response;
        }
        UserVerify userVerify = userVerifies.get(0);
        String username = userVerify.getUsername();
        //比较user_verify查询出 username 和request的 username
        if (!request.getUserName().equals(username)){
            response.setCode(SysRetCodeConstants.USER_INFOR_INVALID.getCode());
            response.setMsg(SysRetCodeConstants.USER_INFOR_INVALID.getMessage());
            return response;
        }
        //更新user_verify表
        userVerify.setIsVerify("Y");
        int updateAffect = userVerifyMapper.updateByPrimaryKey(userVerify);
        if (updateAffect != 1){
            response.setCode(SysRetCodeConstants.USER_INFOR_UPDATE_FAIL.getCode());
            response.setMsg(SysRetCodeConstants.USER_INFOR_UPDATE_FAIL.getMessage());
            return response;
        }
        //通过username去查询 更新member表
        Example memberExample = new Example(Member.class);
        memberExample.createCriteria().andEqualTo("username",username);
        List<Member> members = memberMapper.selectByExample(memberExample);
        if (CollectionUtils.isEmpty(members)){
            response.setCode(SysRetCodeConstants.USER_INFOR_UPDATE_FAIL.getCode());
            response.setMsg(SysRetCodeConstants.USER_INFOR_UPDATE_FAIL.getMessage());
            return response;
        }
        Member member = members.get(0);
        member.setIsVerified("Y");
        int updateMember = memberMapper.updateByPrimaryKey(member);

        if (updateMember != 1){
            response.setCode(SysRetCodeConstants.USER_INFOR_UPDATE_FAIL.getCode());
            response.setMsg(SysRetCodeConstants.USER_INFOR_UPDATE_FAIL.getMessage());
            return response;
        }

        response.setCode(SysRetCodeConstants.SUCCESS.getCode());
        response.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
        return response;
    }
}
