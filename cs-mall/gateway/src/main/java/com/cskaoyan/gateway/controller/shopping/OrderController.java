package com.cskaoyan.gateway.controller.shopping;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.order.OrderCoreService;
import com.mall.order.OrderQueryService;
import com.mall.order.ShoppingOrderService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dto.*;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.intercepter.TokenIntercepter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @Classname ShoppingOrderController
 * @Description TODO
 * @Date 2020/6/11 22:03
 * @Created by Cyuer
 */
@RestController
@Api(tags = "ShoppingOrderController", description = "商品展示")
@RequestMapping("/shopping")
public class OrderController {
    @Reference(timeout = 3000,check = false)
    ShoppingOrderService shoppingOrderService;
    @Reference(timeout = 3000, check = false)
    OrderCoreService orderCoreService;
    @Reference(timeout = 3000, check = false)
    OrderQueryService orderQueryService;

    @GetMapping("/order/{id}")
    public ResponseData ShoppingOrder(@PathVariable("id") Long id,HttpServletRequest httprequest){
        //获得用户的nickname 通过token
        String userinfo = (String) httprequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        OrderItemRequest request = new OrderItemRequest();
        request.setOrderItemId(String.valueOf(id));
        ShoppingOrderDto response =shoppingOrderService.ShopingOrder(request,userinfo);
        return new ResponseUtil<ShoppingOrderDto>().setData(response, SysRetCodeConstants.SUCCESS.getMessage());
    }
    @PostMapping("/order")
    @ApiOperation("创建订单")
    public ResponseData createOrder(@RequestBody CreateOrderRequest createOrderRequest,HttpServletRequest servletRequest) {
        String userInfo = (String) servletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        Long uid = Long.parseLong(object.get("uid").toString());
        createOrderRequest.setUserId(uid);
        CreateOrderResponse createOrderResponse = orderCoreService.createOrder(createOrderRequest);
        if (!OrderRetCode.SUCCESS.getCode().equals(createOrderResponse.getCode()))
            return new ResponseUtil<>().setErrorMsg(createOrderResponse.getMsg());
        return new ResponseUtil<>().setData(createOrderResponse.getOrderId());
    }

    @GetMapping("/order")
    @ApiOperation("获取当前用户的所有订单")
    public ResponseData getAllOrders(Integer size, Integer page, HttpServletRequest servletRequest) {
        String userInfo = (String) servletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        Long uid = Long.parseLong(object.get("uid").toString());
        OrderListRequest orderListRequest = new OrderListRequest();

        orderListRequest.setSize(size);
        orderListRequest.setPage(page);
        orderListRequest.setUserId(uid);

        OrderListResponse orderListResponse = orderQueryService.queryOrderList(orderListRequest);
        if (!orderListResponse.getCode().equals(OrderRetCode.SUCCESS.getCode()))
            return new ResponseUtil<>().setErrorMsg(orderListResponse.getMsg());
        HashMap<String, Object> data = new HashMap<>();
        data.put("data", orderListResponse.getDetailInfoList());
        return new ResponseUtil<>().setData(data);
    }
    @PutMapping("/order/{id}")
    @ApiOperation("取消订单")
    public ResponseData cancleOrder(@PathVariable("id") Long orderId, HttpServletRequest servletRequest) {
        String userInfo = (String) servletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        Long uid = Long.parseLong(object.get("uid").toString());
        CancelOrderRequest request = new CancelOrderRequest(orderId.toString(), uid);
        CancelOrderResponse response = orderQueryService.cancelOrder(request);
        if (!response.getCode().equals(OrderRetCode.SUCCESS.getCode()))
            return new ResponseUtil<>().setErrorMsg(response.getMsg());
        return new ResponseUtil<>().setData(response.getResult());
    }

    @DeleteMapping("/order/{id}")
    @ApiOperation("删除订单")
    public ResponseData deleteOrder(@PathVariable("id") String orderId, HttpServletRequest servletRequest) {
        String userInfo = (String) servletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        Long uid = Long.parseLong(object.get("uid").toString());
        DeleteOrderRequest request = new DeleteOrderRequest(orderId, uid);
        DeleteOrderResponse response = orderQueryService.deleteOrder(request);
        if (!response.getCode().equals(OrderRetCode.SUCCESS.getCode()))
            return new ResponseUtil<>().setErrorMsg(response.getMsg());
        return new ResponseUtil<>().setData(response.getResult());
    }
}
