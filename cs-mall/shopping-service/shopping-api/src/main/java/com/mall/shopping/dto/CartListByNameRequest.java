package com.mall.shopping.dto;

import com.mall.commons.result.AbstractRequest;
import lombok.Data;

/**
 * @Classname CartListByNameRequest
 * @Description TODO
 * @Date 2020/6/13 4:14
 * @Created by Cyuer
 */
@Data
public class CartListByNameRequest extends AbstractRequest {
    private String  username;
    @Override
    public void requestCheck() {

    }
}
