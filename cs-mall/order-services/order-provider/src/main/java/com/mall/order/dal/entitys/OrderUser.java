package com.mall.order.dal.entitys;

import lombok.Data;

import java.util.List;

/**
 * @Classname OrderUser
 * @Description TODO
 * @Date 2020/6/12 18:18
 * @Created by Cyuer
 */

@Data
public class OrderUser<T> {
    private String userName;
    private int orderTotal;
    private int userId;
    private String tel;
    private String streetName;
    private List<T> goodsList;
}
