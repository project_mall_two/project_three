package com.mall.order.services;

import com.mall.order.ShoppingOrderService;
import com.mall.order.converter.OrderConverter;
import com.mall.order.dal.entitys.OrderItem;
import com.mall.order.dal.entitys.OrderShipping;
import com.mall.order.dal.entitys.OrderUser;
import com.mall.order.dal.persistence.OrderItemMapper;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dal.persistence.OrderShippingMapper;
import com.mall.order.dto.*;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Classname OrderShoppingOrderServiceImpl
 * @Description TODO
 * @Date 2020/6/12 17:17
 * @Created by Cyuer
 */
@Slf4j
@Component
@Service
public class OrderShoppingOrderServiceImpl implements ShoppingOrderService {
    @Autowired
    OrderItemMapper orderItemMapper;
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    OrderConverter orderConverter;
    @Override
    public ShoppingOrderDto ShopingOrder(OrderItemRequest request, String userinfo) {
        //由于没有建立相应的dto使用的是OrderItemRequest这个dto这里存储的OrderItemId实际上是OrderId
        Example example =new Example(OrderItem.class);
        Example.Criteria criteria =example.createCriteria();
        criteria.andEqualTo("orderId",request.getOrderItemId());
        List<OrderItem> orderItems = orderItemMapper.selectByExample(example);
        //将数据库转换程的DO和DTO相互转换
        List<OrderItemDto> orderItemDtos = orderConverter.item2dto(orderItems);
        //****注意是查找下单人员信息并非是收货人信息*******
        OrderUser orderUser = orderMapper.countUserOrder(userinfo);
        //将数据库转换程的DO和DTO相互转换
        ShoppingOrderDto shoppingOrderDto = orderConverter.OrderUser2ShoppingOrderDto(orderUser);
        shoppingOrderDto.setGoodsList(orderItemDtos);
        return shoppingOrderDto;
    }
}
