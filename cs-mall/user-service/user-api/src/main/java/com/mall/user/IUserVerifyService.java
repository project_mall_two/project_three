package com.mall.user;

import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;

/**
 * @Classname IUserVerifyService
 * @Description TODO
 * @Date 2020/6/14 16:00
 * @Created by Cyuer
 */
public interface IUserVerifyService {
    UserVerifyResponse verify(UserVerifyRequest request);
}
