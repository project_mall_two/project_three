package com.cskaoyan.gateway;

import com.google.gson.Gson;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.commons.tool.utils.CookieUtil;
import com.mall.shopping.IProductService;
import com.mall.shopping.dto.AllProductRequest;
import com.mall.shopping.dto.AllProductResponse;
import com.mall.user.IKaptchaService;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.KaptchaCodeRequest;
import com.mall.user.dto.KaptchaCodeResponse;
import org.apache.catalina.security.SecurityUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.junit.Before;
import org.junit.internal.requests.ClassRequest;
import org.junit.jupiter.api.Test;
import org.junit.runner.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.support.HttpRequestWrapper;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.mock.http.client.MockClientHttpRequest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.security.Security;

@SpringBootTest
class GatewayApplicationTests {


    @Autowired
    MockHttpServletRequest request;

    @Autowired
    MockHttpServletResponse response;

    @Autowired
    MockHttpSession session;

    @Reference(timeout = 3000, check = false)
    IKaptchaService kaptchaService;

    @Reference(timeout = 3000, check = false)
    IProductService iProductService;

    @Before
    public void init() {
        request = new MockHttpServletRequest();
        //设置字符编码
        request.setCharacterEncoding("UTF-8");
        response = new MockHttpServletResponse();
        //如果使用session则创建一个session
        session = new MockHttpSession();
//        Cookie cookie =new Cookie("access_token","eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ3bGd6cyIsImV4cCI6MTU5MTk2ODAxOCwidXNlciI6IjRCNUM1RkY5OTQ2RjZCQ0M3NkNGODI2RTlCN0UwQjVBQ0Y1RkE0QkNFNzNCRTQ2MkI0QjQ4OTUzNkZEMTZBNjVCNkMwMDY0ODA1N0JDMjlFRDBFMjRCQkVDMDg5MzZGRkIwRkRFMDI2OUJFQzNDMjZGMzdGNkQ3NjY2RjRBN0RFNDNFNUY2RURBNjk3NDg3RTFBNzk4NDdDNjE3Nzg3MEVGODAxRTRBNEQ2RUREMTdCQkE4ODIwODI4NTM3MjJBQUExNDYyQTU2MzVDMTFDOUQ4QTFFMDNBNUY3MzE3MEQwNzIxQ0E1RUYwNzc4OUE2NDkyN0Q4NTg2NEE4QzBCREUifQ.bwf5Bwgus2CCsh3ezSC0f9tt_cafnNLwnFNUpQJ25FE")
    }

    /*    @Test
        public void contextLoads() {
            //需要使用session时候
            request.setSession(session);
            //Request中夹杂Cookie时候
            request.addHeader("Cookie",CookieObeject);
            //request中需要传递一些信息时
            request.setAttribute(key , valueObject);
            request.addParameter(key, value);
            KaptchaCodeRequest request = new KaptchaCodeRequest();
            String code = "";
            String uuid = CookieUtil.getCookieValue((HttpServletRequest) request, "kaptcha_uuid");
            request.setUuid(uuid);
            request.setCode(code);
            KaptchaCodeResponse response = kaptchaService.validateKaptchaCode(request);
            if (response.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())) {

            }

        }*/
    @Test
    public void ShoppingGoodsTest(
    ) {
        AllProductRequest allProductRequest = new AllProductRequest();
        allProductRequest.setPage(1);
        allProductRequest.setSize(20);
        allProductRequest.setSort("1");
        allProductRequest.setPriceGt(0.0);
        allProductRequest.setPriceLte(null);
        AllProductResponse response = iProductService.getAllProduct(allProductRequest);
        ResponseData responseDate = new ResponseUtil<AllProductResponse>().setData(response, response.getMsg());
        System.out.println(new Gson().toJson(responseDate));
    }
}
