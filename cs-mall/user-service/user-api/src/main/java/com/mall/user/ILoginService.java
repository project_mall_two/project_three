package com.mall.user;

import com.mall.user.dto.CheckAuthRequest;
import com.mall.user.dto.CheckAuthResponse;
import com.mall.user.dto.UserLoginRequest;
import com.mall.user.dto.UserLoginResponse;

/**
 * @Classname IUserLoginService
 * @Description TODO
 * @Date 2020/6/13 4:22
 * @Created by Cyuer
 */
public interface ILoginService {
    UserLoginResponse login(UserLoginRequest request);
    CheckAuthResponse validToken(CheckAuthRequest checkAuthRequest);
}
