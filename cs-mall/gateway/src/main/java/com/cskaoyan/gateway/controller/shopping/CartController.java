package com.cskaoyan.gateway.controller.shopping;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.ICartService;
import com.mall.shopping.dto.*;
import com.mall.user.intercepter.TokenIntercepter;
import io.swagger.annotations.*;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @Classname CartController
 * @Description TODO
 * @Date 2020/6/12 21:21
 * @Created by Cyuer
 */
@RestController
@Api(tags = "CartController", description = "购物车控制层")
public class CartController {
    @Reference(timeout = 3000, check = false)
    ICartService iCartService;


    @GetMapping("/shopping/carts")
    @ApiOperation("显示购物车中的商品")
    public ResponseData showCartsList(HttpServletRequest request) {
        String userInfo = (String) request.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        Long uid = Long.parseLong(object.get("uid").toString());
        CartListByIdRequest cartListByIdRequest = new CartListByIdRequest();
        cartListByIdRequest.setUserId(uid);
        CartListByIdResponse cartListByIdResponse = iCartService.getCartListById(cartListByIdRequest);
        return new ResponseUtil<List<CartProductDto>>().setData(cartListByIdResponse.getCartProductDtos());
    }


    @PostMapping("/shopping/carts")
    @ApiOperation("选中购物车中的商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户ID", paramType = "Long"),
            @ApiImplicitParam(name = "productId", value = "产品ID", paramType = "Long"),
            @ApiImplicitParam(name = "productNum", value = "产品数量", paramType = "Integer"),
    })
    public ResponseData addProductCart(@RequestBody Map<String,Object> map
    ) {
        AddCartRequest addCartRequest = new AddCartRequest();
        addCartRequest.setUserId(Long.valueOf((String) map.get("userId")));
        addCartRequest.setItemId(Long.valueOf((Integer) map.get("productId")));
        addCartRequest.setNum((Integer) map.get("productNum"));
        AddCartResponse cartResponse = iCartService.addToCart(addCartRequest);
        return new ResponseUtil<AddCartResponse>().setData(cartResponse);
    }

    @PutMapping("/shopping/carts")
    @ApiOperation("更新购物车中的商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户ID", paramType = "Long"),
            @ApiImplicitParam(name = "productId", value = "产品ID", paramType = "Long"),
            @ApiImplicitParam(name = "productNum", value = "产品数量", paramType = "Integer"),
            @ApiImplicitParam(name = "checked", value = "产品数量", paramType = "String")
    })
    public ResponseData updateProductCart(
            @RequestBody UpdateCartNumRequest updateCartNumRequest
    ) {
        UpdateCartNumResponse updateCartNumResponse = iCartService.updateCartNum(updateCartNumRequest);
        return new ResponseUtil<UpdateCartNumResponse>().setData(updateCartNumResponse);
    }


    @DeleteMapping("/shopping/carts/{uid}/{pid}")
    @ApiOperation("删除选中购物车中的产品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "用户ID", paramType = "Long"),
            @ApiImplicitParam(name = "pid", value = "产品ID", paramType = "Long"),
    })
    public ResponseData deleteProductCart(@PathVariable("uid") Long uid,
                                          @PathVariable("pid") Long pid
    ) {
        DeleteCartItemRequest deleteCartItemRequest = new DeleteCartItemRequest();
        deleteCartItemRequest.setItemId(pid);
        deleteCartItemRequest.setUserId(uid);
        DeleteCartItemResponse deleteCartItemResponse = iCartService.deleteCartItem(deleteCartItemRequest);
        return new ResponseUtil<DeleteCartItemResponse>().setData(deleteCartItemResponse);
    }


    @DeleteMapping("/shopping/items/{id}")
    @ApiOperation("删除购物车所有的产品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", paramType = "Long"),
    })
    public ResponseData deleteAllProductCart(@PathVariable("id") Long id
    ) {
        DeleteCheckedItemRequest deleteCheckedItemRequest = new DeleteCheckedItemRequest();
        deleteCheckedItemRequest.setUserId(id);
        DeleteCheckedItemResposne deleteCheckedItemResposne = iCartService.deleteCheckedItem(deleteCheckedItemRequest);
        return new ResponseUtil<DeleteCheckedItemResposne>().setData(deleteCheckedItemResposne);
    }

}
