package com.mall.shopping.converter;

import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.entitys.ItemDesc;
import com.mall.shopping.dto.ProductDetailDto;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.Arrays;

/**
 * @Classname ProductDetailConverter
 * @Description TODO
 * @Date 2020/6/14 15:38
 * @Created by Cyuer
 */
public class ProductDetailConverter {
    public static ProductDetailDto item2Dto(Item item, ItemDesc itemDesc){
        ProductDetailDto productDetailDto=new ProductDetailDto();
        productDetailDto.setProductId(item.getId());
        productDetailDto.setProductName(item.getTitle());
        productDetailDto.setLimitNum(item.getLimitNum());
        productDetailDto.setSalePrice(item.getPrice());
        productDetailDto.setSubTitle(item.getTitle());
        String[] images = item.getImages();
        productDetailDto.setProductImageSmall(Arrays.asList(images));
        productDetailDto.setProductImageBig(item.getImageBig());
        productDetailDto.setDetail(itemDesc.getItemDesc());
        return productDetailDto;
    }
    public static ProductDetailDto item2Dto(Item item){
        ProductDetailDto productDetailDto=new ProductDetailDto();
        productDetailDto.setProductId(item.getId());
        productDetailDto.setProductName(item.getTitle());
        productDetailDto.setLimitNum(item.getLimitNum());
        productDetailDto.setSalePrice(item.getPrice());
        productDetailDto.setSubTitle(item.getTitle());
        String[] images = item.getImages();
        productDetailDto.setProductImageSmall(Arrays.asList(images));
        productDetailDto.setProductImageBig(item.getImageBig());
        return productDetailDto;
    }
    public static ProductDetailDto item2Dto(ItemDesc itemDesc){
        ProductDetailDto productDetailDto=new ProductDetailDto();
        productDetailDto.setDetail(itemDesc.getItemDesc());
        return productDetailDto;
    }
}
