package com.cskaoyan.gateway.controller.user;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.user.IVerifyService;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @Author lee
 * @Date 2020/6/12 22:12
 * @Version 1.0
 */
@RestController
@RequestMapping("user")
public class UserVerifyController {

    @Reference
    private IVerifyService verifyService;

    @GetMapping("verify")
    @Anoymous
    public ResponseData verify(Map<String,String> map, HttpServletRequest request, HttpServletResponse response){
        String uuid = map.get("uuid");
        String username = map.get("username");
        //验证
        if(StringUtils.isEmpty(uuid) || StringUtils.isEmpty(username)) {
            return new ResponseUtil<>().setErrorMsg("注册用户名序列号不能为空");
        }

        UserVerifyRequest userVerifyRequest = new UserVerifyRequest();
        userVerifyRequest.setUuid(uuid);
        userVerifyRequest.setUserName(username);
        //修改数据库
        UserVerifyResponse verifyResponse = verifyService.verify(userVerifyRequest);

        if (!SysRetCodeConstants.SUCCESS.getCode().equals(verifyResponse.getCode())){
            return new ResponseUtil<>().setErrorMsg(verifyResponse.getMsg());
        }
        return new ResponseUtil<>().setData(null);
    }
}
