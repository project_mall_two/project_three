package com.mall.user;

import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;

/**
 * @Author lee
 * @Date 2020/6/12 22:55
 * @Version 1.0
 */

public interface IVerifyService {
    UserVerifyResponse verify(UserVerifyRequest userVerifyRequest);
}
