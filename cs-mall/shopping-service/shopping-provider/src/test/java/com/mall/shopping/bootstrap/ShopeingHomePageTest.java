package com.mall.shopping.bootstrap;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.gson.Gson;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dal.persistence.PanelMapper;
import com.mall.shopping.dto.HomePageResponse;
import com.mall.shopping.dto.PanelContentItemDto;
import com.mall.shopping.dto.PanelDto;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Classname ShopeingHomePageTest
 * @Description TODO
 * @Date 2020/6/11 18:19
 * @Created by Cyuer
 */
public class ShopeingHomePageTest extends ShoppingProviderApplicationTests{
    @Autowired
    PanelMapper panelMapper;
    @Autowired
    PanelContentMapper panelContentMapper;
    @Autowired
    ContentConverter contentConverter;
    @Test
    public void HomepageTest(){

        Example example = new Example(Panel.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("position", 0);
        criteria.andEqualTo("status", 1);
        example.setOrderByClause("sort_order");
        List<Panel> panels = panelMapper.selectByExample(example);
        List<PanelDto> panelDtos = contentConverter.panels2Dto(panels);
        for (PanelDto p : panelDtos) {
            //根据panel表中limitnum来限制查询的内容数量
            PageHelper.startPage(0, p.getLimitNum());
            List<PanelContentItem> panelContentItems = panelContentMapper.selectPanelContentAndProductWithPanelId(p.getId());
            PageInfo<PanelContentItem> pageInfo = new PageInfo<>(panelContentItems);
            //将查询好的list数据转换程dto发送到前端
            List<PanelContentItemDto> panelContentItemDtos = contentConverter.panelContentItem2Dto(panelContentItems);
            p.setPanelContentItems(panelContentItemDtos);
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=");
//        System.out.println();
//        System.out.println(panels.toString());
//        for(PanelDto p :panelDtos){
//            System.out.println(p.getPanelContentItems());
//        }
        HomePageResponse homePageResponse = new HomePageResponse();
        homePageResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
        homePageResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        Gson gson =new Gson();
        System.out.println(gson.toJson(homePageResponse));

    }

}
