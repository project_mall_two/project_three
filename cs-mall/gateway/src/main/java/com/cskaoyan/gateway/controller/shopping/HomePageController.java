package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IContentService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.HomePageResponse;
import com.mall.shopping.ShopingHomepageService;
import com.mall.shopping.dto.NavListResponse;
import com.mall.shopping.dto.PanelDto;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import io.swagger.annotations.Api;

import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Classname HomePageController
 * @Description TODO 实现接口shopping/homepage
 * @Created by Cyuer
 *
 */
@RestController
@Api(tags = "HomePageController",description = "商品首页控制")
public class HomePageController {
    @Reference(timeout = 3000,check = false)
    ShopingHomepageService shopingHomepageService;

    @Reference(timeout = 3000,check = false)
    IContentService contentService;

    @Anoymous
    @GetMapping("/shopping/homepage")
    @ApiOperation("页面显示")
    public ResponseData ShoppingHomepage(){
        ResponseData responseData = new ResponseData();
        HomePageResponse response = shopingHomepageService.ShoppingHomepage();
        if(SysRetCodeConstants.SUCCESS.getCode().equals(response.getCode())){
            return  new ResponseUtil<List<PanelDto>>().setData(response.getPanelContentItemDtos());
        }
        return new ResponseUtil<String>().setData(response.getMsg());
    }

    @Anoymous
    @GetMapping("/shopping/navigation")
    @ApiOperation("导航栏显示")
    public ResponseData navigation(){
        NavListResponse response = contentService.queryNavList();
        if (response.getCode().equals(ShoppingRetCode.SUCCESS.getCode())){
            return new ResponseUtil().setData(response.getPannelContentDtos());
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }
}
