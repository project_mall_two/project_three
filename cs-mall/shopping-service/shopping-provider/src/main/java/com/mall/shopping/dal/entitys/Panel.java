package com.mall.shopping.dal.entitys;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Table(name = "tb_panel")
public class Panel implements Serializable {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer id;
    @Column(name="name")
    private String name;
    @Column(name="type")
    private Integer type;
    @Column(name="sort_order")
    private Integer sortOrder;
    @Column(name="position")
    private Integer position;
    @Column(name="limit_num")
    private Integer limitNum;
    @Column(name="status")
    private Integer status;
    @Column(name="remark")
    private String remark;
    @Column(name="created")
    private Date created;
    @Column(name="updated")
    private Date updated;

    private List<PanelContentItem> panelContentItems;

    @Transient
    private Long productId;

    private static final long serialVersionUID = 1L;

}