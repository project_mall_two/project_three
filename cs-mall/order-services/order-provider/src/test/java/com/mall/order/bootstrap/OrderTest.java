//package com.mall.order.bootstrap;
//
//import com.google.gson.Gson;
//import com.mall.order.converter.OrderConverter;
//import com.mall.order.dal.entitys.OrderItem;
//import com.mall.order.dal.entitys.OrderUser;
//import com.mall.order.dal.persistence.OrderItemMapper;
//import com.mall.order.dal.persistence.OrderMapper;
//import com.mall.order.dto.OrderItemDto;
//import com.mall.order.dto.OrderItemRequest;
//import com.mall.order.dto.ShoppingOrderDto;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import tk.mybatis.mapper.entity.Example;
//
//import java.util.List;
//
///**
// * @Classname OrderTest
// * @Description TODO
// * @Date 2020/6/12 18:34
// * @Created by Cyuer
// */
//public class OrderTest extends OrderProviderApplicationTests{
//    @Autowired
//    OrderItemMapper orderItemMapper;
//    @Autowired
//    OrderMapper orderMapper;
//    @Autowired
//    OrderConverter orderConverter;
//    @Test
//    public void ShopingOrder() {
//
//        OrderItemRequest request =new OrderItemRequest();
//        request.setOrderItemId("20041912580013939");
//        String userinfo ="admin";
//        //由于没有建立相应的dto使用的是OrderItemRequest这个dto这里存储的OrderItemId实际上是OrderId
//        Example example =new Example(OrderItem.class);
//        Example.Criteria criteria =example.createCriteria();
//        criteria.andEqualTo("orderId",request.getOrderItemId());
//        List<OrderItem> orderItems = orderItemMapper.selectByExample(example);
//        //将数据库转换程的DO和DTO相互转换
//        List<OrderItemDto> orderItemDtos = orderConverter.item2dto(orderItems);
//        //****注意是查找下单人员信息并非是收货人信息*******
//        OrderUser orderUser = orderMapper.countUserOrder(userinfo);
//        //将数据库转换程的DO和DTO相互转换
//        ShoppingOrderDto shoppingOrderDto = orderConverter.OrderUser2ShoppingOrderDto(orderUser);
//        shoppingOrderDto.setGoodsList(orderItemDtos);
//        System.out.println(new Gson().toJson(shoppingOrderDto));
//    }
//}
