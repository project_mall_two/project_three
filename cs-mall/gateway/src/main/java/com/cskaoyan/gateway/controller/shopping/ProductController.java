package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IProductService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.ProductDetailDto;
import com.mall.shopping.dto.ProductDetailRequest;
import com.mall.shopping.dto.ProductDetailResponse;
import com.mall.shopping.dto.RecommendResponse;
import com.mall.user.annotation.Anoymous;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HaiqingSun
 * @version 1.0
 * @date 2020/6/13 0:35
 */
@RestController
@Api(tags = "ProductController",description = "Recommend控制层")
public class ProductController {

    @Reference(timeout = 3000, check = false)
    IProductService iProductService;

    @RequestMapping("shopping/recommend")
    @Anoymous
    @ApiOperation("获取推荐商品")
    public ResponseData getRecommendGoods() {
        RecommendResponse recommendGoods = iProductService.getRecommendGoods();
        if (recommendGoods.getPanelContentItemDtos() == null) {
            return new ResponseUtil<>().setErrorMsg(ShoppingRetCode.SYSTEM_ERROR.getCode());
        }
        return new ResponseUtil<>().setData(recommendGoods.getPanelContentItemDtos());
    }

    @GetMapping("shopping/product/{pid}")
    @Anoymous
    @ApiOperation("商品详情")
    public ResponseData ProductDetail(@PathVariable("pid") Long pid) {
        ProductDetailRequest request = new ProductDetailRequest();
        request.setId(pid);
        ProductDetailResponse response = iProductService.getProductDetail(request);
        return new ResponseUtil<ProductDetailDto>().setData(response.getProductDetailDto());
    }
}
