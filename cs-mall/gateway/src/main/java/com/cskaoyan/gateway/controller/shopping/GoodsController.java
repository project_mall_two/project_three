package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IProductService;
import com.mall.shopping.dto.AllProductRequest;
import com.mall.shopping.dto.AllProductResponse;
import com.mall.shopping.dto.ProductDetailRequest;
import com.mall.shopping.dto.ProductDetailResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Classname GoodsController
 * @Description TODO
 * @Date 2020/6/12 14:17
 * @Created by Cyuer
 */
@RestController
@Api(tags = "GoodsController",description = "商品控制层")
public class GoodsController {
    @Reference(timeout = 300 ,check = false)
    IProductService iProductService;
    @GetMapping("/shopping/goods")
    @ApiOperation("获得所有商品（分页）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "Integer"),
            @ApiImplicitParam(name = "size", value = "每页条数", paramType = "Integer"),
            @ApiImplicitParam(name = "sort", value = "排序", paramType = "Integer"),
            @ApiImplicitParam(name = "priceGt ", value = "最小价格", paramType = "Double"),
            @ApiImplicitParam(name = "priceLte", value = "最大价格", paramType = "Double")
    })
    public ResponseData ShoppingGoods(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", defaultValue = "20") Integer size,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "priceGt",defaultValue = "0" , required = false) Double priceGt,
            @RequestParam(value = "priceLte",  required = false) Double priceLte
    ){
        AllProductRequest allProductRequest = new AllProductRequest();
        allProductRequest.setPage(page);
        allProductRequest.setSize(size);
        allProductRequest.setSort(sort);
        allProductRequest.setPriceGt(priceGt);
        allProductRequest.setPriceLte(priceLte);
        AllProductResponse response = iProductService.getAllProduct(allProductRequest);
        Map<String,Object> result = new HashMap<>();
        result.put("total",response.getTotal());
        result.put("data",response.getProductDtoList());
        return new ResponseUtil<Map>().setData(result);
    }
    @GetMapping("/shopping/goods/{id}")
    @ApiOperation("查看商品详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "商品id", paramType = "Long"),
    })
    public ResponseData GoodDetails(@PathVariable(name= "id") Long id){
        ProductDetailRequest productDetailRequest = new ProductDetailRequest();
        productDetailRequest.setId(id);
        ProductDetailResponse productDetailResponse = iProductService.getProductDetail(productDetailRequest);
        return  new ResponseUtil<ProductDetailResponse>().setData(productDetailResponse);
    }

}
