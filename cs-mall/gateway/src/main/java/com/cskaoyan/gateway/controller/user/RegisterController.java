package com.cskaoyan.gateway.controller.user;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.commons.tool.utils.CookieUtil;
import com.mall.user.IKaptchaService;
import com.mall.user.IRegisterService;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.KaptchaCodeRequest;
import com.mall.user.dto.KaptchaCodeResponse;
import com.mall.user.dto.UserRegisterRequest;
import com.mall.user.dto.UserRegisterResponse;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Author lee
 * @Date 2020/6/11 20:59
 * @Version 1.0
 */
@RestController
@RequestMapping("user")
public class RegisterController {

    @Reference
    private IKaptchaService kaptchaService;

    @Reference
    private IRegisterService registerService;

    @PostMapping("register")
    @Anoymous
    public ResponseData register(@RequestBody Map<String,String> map, HttpServletRequest request){

        //从请求体的map中获取数据
        String userName = map.get("userName");
        String userPwd = map.get("userPwd");
        String captcha = map.get("captcha");
        String email = map.get("email");

        //验证验证码
        //iKaptchaService.validateKaptchaCode(kaptchaCodeRequest)传入的是一个 kaptchaCodeRequest,所以我们需要new一个
        KaptchaCodeRequest kaptchaCodeRequest = new KaptchaCodeRequest();
        String uuid = CookieUtil.getCookieValue(request, "kaptcha_uuid");//从请求报文获取生成验证码时产生的uuid
        kaptchaCodeRequest.setUuid(uuid);
        kaptchaCodeRequest.setCode(captcha);

        //验证验证码，调用了iKaptchaService的验证方法
        KaptchaCodeResponse kaptchaCodeResponse = kaptchaService.validateKaptchaCode(kaptchaCodeRequest);

        //判断验证验证码的结果
        String code = kaptchaCodeResponse.getCode();
        if(!SysRetCodeConstants.SUCCESS.getCode().equals(code)){
            return new ResponseUtil<>().setErrorMsg(kaptchaCodeResponse.getMsg());
        }

        //搞一个userRegisterRequest，因为我们调用service需要userRegisterRequest
        UserRegisterRequest userRegisterRequest = new UserRegisterRequest();
        userRegisterRequest.setUserName(userName);
        userRegisterRequest.setUserPwd(userPwd);
        userRegisterRequest.setEmail(email);

        //调用service并返回相应的registerResponse
        UserRegisterResponse registerResponse = registerService.register(userRegisterRequest);

        //判断service的返回结果，并向客户端返回相应的结果
        if(!SysRetCodeConstants.SUCCESS.getCode().equals(registerResponse.getCode())){
            return new ResponseUtil<>().setErrorMsg(registerResponse.getMsg());
        }
        return new ResponseUtil<>().setData(null);
    }
}
