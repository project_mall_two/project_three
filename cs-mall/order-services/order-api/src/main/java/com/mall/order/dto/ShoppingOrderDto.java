package com.mall.order.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Classname ShoppingOrderDto
 * @Description TODO
 * @Date 2020/6/12 17:29
 * @Created by Cyuer
 */
@Data
public class ShoppingOrderDto<T> implements Serializable {

    /**
     * userName : test
     * orderTotal : 999
     * userId : 62
     * goodsList : [{"id":"20061216523338223","itemId":"100053202","orderId":"20061216523335662","num":1,"title":"地平线 8 号商务旅行箱","price":999,"totalFee":999,"picPath":"https://resource.smartisan.com/resource/d1dcca9144e8d13ffb33026148599d0a.png"}]
     * tel : 18782059038
     * streetName : 上海青浦区汇联路
     */

    private String userName;
    private int orderTotal;
    private int userId;
    private String tel;
    private String streetName;
    private List<T> goodsList;

}
